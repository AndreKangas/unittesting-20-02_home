function validate_is_number_inputs(a, b) {
    if (isNaN(a) || isNaN(b)) {
        throw new Error("invalid input")
    }
    if (typeof a === 'string' || typeof b === 'string') {
        throw new Error("input need to be number")
    }
}

module.exports = validate_is_number_inputs