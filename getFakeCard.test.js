const getFakeCard = require('./getFakeCard');

describe('get fake card', () => {
    it ('id=123 is Clara Keebler', () => {
        const card = getFakeCard(123);
        expect(card.name).toBe('Clara Keebler');

        expect(card).toMatchSnapshot();
    })

    it ('id=500 is Frank Lebsack', () => {
        const card = getFakeCard(500);
        expect(card.name).toBe('Frank Lebsack');

        expect(card).toMatchSnapshot();
    })
})